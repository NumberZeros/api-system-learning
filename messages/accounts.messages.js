module.exports = {
  NOT_FOUND_USER: "Email or password is not valid",
  NOT_LOGIN: "you must login first",
  EXCUTE_ADMIN: "Only admin excute this function",
  NOT_FOUND_EMAIL: " Email is not found",
  ACCOUNT_BLOCK: "Account is blocked",
  NOT_VALID_CODE: "Code is wrong",
};
